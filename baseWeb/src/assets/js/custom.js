var currentYear = new Date().getFullYear();
var minDate = new Date(currentYear, 0, 1);
var maxDate = new Date(currentYear, 11, 31);

var feriados = [{
    id: 0,
    startDate: new Date(currentYear, 0, 1),
    endDate: new Date(currentYear, 0, 1)
}, {
    id: 1,
    startDate: new Date(currentYear, 3, 19),
    endDate: new Date(currentYear, 3, 20)
}, {
    id: 2,
    startDate: new Date(currentYear, 4, 1),
    endDate: new Date(currentYear, 4, 1)
}, {
    id: 3,
    startDate: new Date(currentYear, 4, 21),
    endDate: new Date(currentYear, 4, 21)
}, {
    id: 4,
    startDate: new Date(currentYear, 5, 7),
    endDate: new Date(currentYear, 5, 7)
}, {
    id: 5,
    startDate: new Date(currentYear, 6, 16),
    endDate: new Date(currentYear, 6, 16)
}, {
    id: 6,
    startDate: new Date(currentYear, 7, 15),
    endDate: new Date(currentYear, 7, 15)
}, {
    id: 7,
    startDate: new Date(currentYear, 7, 20),
    endDate: new Date(currentYear, 7, 20)
}, {
    id: 8,
    startDate: new Date(currentYear, 8, 18),
    endDate: new Date(currentYear, 8, 20)
}, {
    id: 9,
    startDate: new Date(currentYear, 9, 31),
    endDate: new Date(currentYear, 10, 1)
}, {
    id: 10,
    startDate: new Date(currentYear, 11, 25),
    endDate: new Date(currentYear, 11, 25)
}, {
    id: 11,
    startDate: new Date(currentYear, 11, 31),
    endDate: new Date(currentYear, 11, 31)
}];